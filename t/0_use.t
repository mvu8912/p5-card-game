use strict;
use warnings;

use Test::More tests => 3;                      # last test to print

use_ok "Card";
use_ok "Pack";
use_ok "Player";
