use strict;
use warnings;

use Test::More tests => 1;                      # last test to print

use Card;

my $card = Card->new( number => 1, suit => "Clubs" );

is $card->name, $card->suit.'-'.$card->number;
