use strict;
use warnings;

use Test::More tests => 1;                      # last test to print

use Player;

my $player = Player->new( name => "Foobar" );

is $player->name, "Foobar";
