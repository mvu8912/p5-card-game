
use strict;
use warnings;

use Test::More tests => 2;                      # last test to print

use Players;

my $players = Players->new;

is $players->player(0)->name, "Player 1";
is $players->player(1)->name, "Player 2";


