use strict;
use warnings;

use Test::More tests => 4;                      # last test to print

use Pack;

my $pack = Pack->new;

is $pack->n_of_cards, 52, "52 Cards";
is $pack->pick_card(0)->name, "Clubs-A", "Check First Card";
is $pack->pick_card(-1)->name, "Spades-K", "Check Last Card";


{
    my $index = 0;
    foreach my $card($pack->shuffle_cards) {
        $index++;
        if ( $card->name eq "Clubs-A" ) {
            last;
        }
    }
    isnt $index, 0, "After shuffle, the first card will be in order cut";
}

