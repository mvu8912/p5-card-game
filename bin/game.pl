#!/usr/bin/perl 
use strict;
use warnings;
use Game qw( ask_questions play_cards tally_up_player_result );

my %answer  = ask_questions();
my $players = play_cards(%answer);
print "\n" . tally_up_player_result($players) . "\n";

exit;
