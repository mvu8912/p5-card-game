package Players;
use Moose;
use Player;

=head1 NAME

Players - A player collection class

=head1 SYNOPSIS

 use Players

 my $players = Players->new;

=cut

has players => (
    is      => "ro",
    isa     => "ArrayRef[Player]",
    builder => "_builder_players",
    traits  => ['Array'],
    handles => {
        all    => "elements",
        player => "get"
    }
);

sub next_player {
    my $self = shift;
    my $id   = $self->_next_player_id;
    return $self->player($id);
}

sub _builder_players {
    my $self = shift;
    my @players = map { Player->new( name => "Player $_" ) } 1 .. 2;
    return \@players;
}

sub _next_player_id {
    my $self = shift;
    return int rand(2);
}

=head1 AUTHOR

"Michael Vu" <email@michael.vu>

=head1 COPYRIGHT

Copyright 2013-2006 by Michael Vu <email@michael.vu>

This program is only for showing coding styles purpose.

Not for selling or redistribute and/or modify it under the author's authorisation.

=cut

1;
