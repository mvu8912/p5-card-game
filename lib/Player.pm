package Player;
use Moose;

=head1 NAME

Player - A player class

=head1 DESCRIPTION

This claas let you to create a player object, which provides

3 useful interface to interact with the player game data.

* snapped - How many times of snaped

* snaps - Snaps a card

* cards - all the cards has been snapped.

=head1 SYNOPSIS

 use Player;

 my $player = Player->new(name => "Foobar");

=cut

has name => (
    is       => "ro",
    isa      => "Str",
    required => 1,
);

has card => (
    is  => "rw",
    isa => "Card",
);

has snap_history => (
    is      => "ro",
    isa     => "ArrayRef",
    traits  => ['Array'],
    builder => "_build_snap_history",
    handles => {
        snapped => "count",
        snaps   => "push",
        cards   => "elements",
    }
);

sub _build_snap_history {
    return [];
}

sub hold_card {
    my $self = shift;
    $self->card(shift);
}

sub more_than_2_of_this {
    my $self  = shift;
    my $card  = shift;
    my @cards = grep { $card->name eq $_->name } $self->cards;
    return scalar(@cards) >= 2 ? 1 : 0;
}

=head1 AUTHOR

"Michael Vu" <email@michael.vu>

=head1 COPYRIGHT

Copyright 2013-2006 by Michael Vu <email@michael.vu>

This program is only for showing coding styles purpose.

Not for selling or redistribute and/or modify it under the author's authorisation.

=cut

1;
