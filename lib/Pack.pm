package Pack;
use Moose;
use Card;

=head1 NAME

Pack - A card collection class

=head1 SYNOPSIS

 use Pack;

 my $pack_of_cards = Pack->new;

=head1 DESCRIPTION

 This class create a object with 52 cards and you can shuffle the card by
  calling shuffle_cards.

 To create the object. The constructure doesn't need any parameters with.

=head1 METHODS

=head2 shuffle_cards

 Shuffle order of the cards


=cut

has cards => (
    is      => "rw",
    isa     => "ArrayRef[Card]",
    builder => "_build_cards",
    traits  => ['Array'],
    handles => {
        shuffle_cards => "shuffle",
        n_of_cards => "count",
        pick_card => "get",
    }
);

sub _build_cards {
    my $self  = shift;
    my @cards = ();
    foreach my $suit ( _suits() ) {
        foreach my $num ( _card_numbers() ) {
            push @cards, Card->new( suit => $suit, number => $num );
        }
    }
    return \@cards;
}

sub _suits {
    return qw(Clubs Diamonds Hearts Spades);
}

sub _card_numbers {
    return ( 'A', 2 .. 10, qw( J Q K ) );
}

1;
