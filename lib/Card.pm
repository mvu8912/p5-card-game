package Card;
use Moose;

=head1 NAME

Card - This is a class for a card

=head1 SYNOPSIS

 use Card

 Card->new( suit => "Clubs", number => "K" );

=cut

has number => (
    is       => "ro",
    isa      => "Str",
    required => 1,
);

has suit => (
    is       => "ro",
    isa      => "Str",
    required => 1,
);

=head1 METHODS

=head2 name

get the card name

=cut

sub name {
    my $self = shift;
    return $self->suit . '-' . $self->number;
}

=head1 AUTHOR

"Michael Vu" <email@michael.vu>

=head1 COPYRIGHT

Copyright 2013-2006 by Michael Vu <email@michael.vu>

This program is only for showing coding styles purpose.

Not for selling or redistribute and/or modify it under the author's authorisation.

=cut

1;
