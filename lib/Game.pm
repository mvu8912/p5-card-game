package Game;
use strict;
use warnings;
use Pack;
use Players;
use IO::Prompt;
use base "Exporter";
our @EXPORT_OK = qw(ask_questions play_cards tally_up_player_result );

=head1 NAME

Game - Card Game module

=head1 DESCRIPTION

This module provide the interface to build a card game.

=head1 SYNOPSIS

 use Game "ask_questions";

 ask_questions --> ask the user questions before to play

=head1 METHODS 

=head2 ask_questions

Ask 2 questions

=cut

sub ask_questions {
    my $n_of_packs = prompt("How many packs of cards to use? ");

    my @cards = map { $_->shuffle_cards } map { Pack->new } 1 .. $n_of_packs;

    my $total_cards = scalar @cards;

    my %matches = ( a => {}, b => {}, c => {} );

    foreach my $match ( sort keys %matches ) {
        my @matches = ();
        foreach my $iter ( 1 .. 3 ) {
            push @matches, $cards[ int rand($total_cards) ]->name;
        }
        $matches{$match} = { map { $_ => 1 } @matches };
        print "$match. " . join( ", ", @matches ) . "\n";
    }

    my $match = prompt("Which 3 matching to use? ");
    return (
        cards   => \@cards,
        matches => $matches{$match},
    );
}

=head1 play_cards

Play the deck from top to bottom with 2 computer users

=cut

sub play_cards {
    my %args    = @_;
    my @cards   = @{ $args{cards} };
    my %matches = %{ $args{matches} };

    my $players = Players->new;

    foreach my $card (@cards) {
        print "Card: " . $card->name . "\n";
        if ( $matches{ $card->name } ) {
            my $player = $players->next_player;
            print $player->name . " Sapped!\n";
            $player->snaps($card);
            if ( $player->more_than_2_of_this($card) ) {
                print $player->name . " take the ownership\n";
            }
        }
        sleep 1;
    }

    return $players;
}

=head2 tally_up_player_result

Tally up the score of the players

=cut

sub tally_up_player_result {
    my $players = shift;
    my $player1 = $players->player(0);
    my $player2 = $players->player(1);
    if ( $player1->snapped > $player2->snapped ) {
        return "Winner is player 1";
    }
    if ( $player1->snapped == $player2->snapped ) {
        return "Draw!!";
    }
    if ( $player1->snapped < $player2->snapped ) {
        return "Winner is player 2";
    }
}

=head1 AUTHOR

"Michael Vu" <email@michael.vu>

=head1 COPYRIGHT

Copyright 2013-2006 by Michael Vu <email@michael.vu>

This program is only for showing coding styles purpose.

Not for selling or redistribute and/or modify it under the author's authorisation.

=cut

1;
